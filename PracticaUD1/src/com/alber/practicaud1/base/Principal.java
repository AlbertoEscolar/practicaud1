package com.alber.practicaud1.base;


import com.alber.practicaud1.gui.Controlador;
import com.alber.practicaud1.gui.Vista;

public class Principal {

    static Vista vista;
    static Controlador controlador;

    public static void main(String[] args) {
        Vista vista = new Vista(controlador);
        ModeloAreopuerto modeloAreopuerto = new ModeloAreopuerto();
        Controlador controlador = new Controlador(vista, modeloAreopuerto);

    }


}
