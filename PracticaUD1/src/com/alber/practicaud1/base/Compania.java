package com.alber.practicaud1.base;

import java.io.Serializable;

public class Compania implements Serializable {
    //Clase que muestra unicamente las propiedades de el objeto
    String nombreCompania;
    String ciudad;
    int empleados;
    int valor;
    int clientes;

    public Compania() {
    }

    public Compania(String nombreCompania, String ciudad, int empleados, int valor, int clientes) {
        this.nombreCompania = nombreCompania;
        this.ciudad = ciudad;
        this.empleados = empleados;
        this.valor = valor;
        this.clientes = clientes;
    }

    //Getters y Setters
    public String getNombreCompania() {
        return nombreCompania;
    }

    public void setNombreCompania(String nombreCompania) {
        this.nombreCompania = nombreCompania;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getEmpleados() {
        return empleados;
    }

    public void setEmpleados(int empleados) {
        this.empleados = empleados;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public int getClientes() {
        return clientes;
    }

    public void setClientes(int clientes) {
        this.clientes = clientes;
    }

    //To string que devuelve los campos principales
    @Override
    public String toString() {
        return "Nombre = " + nombreCompania + " ";
    }
}
