package com.alber.practicaud1.base;

import java.io.Serializable;
import java.time.LocalDate;

public class Avion implements Serializable {
    //Clase que muestra unicamente las propiedades de el objeto
    String modelo;
    String id;
    int numeroPasajeros;
    LocalDate anoModelo;
    boolean tipo;
    Destino destino;

    public Avion() {
    }

    public Avion(String modelo, String id, int numeroPasajeros, LocalDate anoModelo, boolean tipo) {
        this.modelo = modelo;
        this.id = id;
        this.numeroPasajeros = numeroPasajeros;
        this.anoModelo = anoModelo;
        this.tipo = tipo;
    }

    public Avion(String modelo, String id, int numeroPasajeros, LocalDate anoModelo, boolean tipo, Destino destino) {
        this.modelo = modelo;
        this.id = id;
        this.numeroPasajeros = numeroPasajeros;
        this.anoModelo = anoModelo;
        this.tipo = tipo;
        this.destino = destino;
    }

    //Getters y Setters
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getNumeroPasajeros() {
        return numeroPasajeros;
    }

    public void setNumeroPasajeros(int numeroPasajeros) {
        this.numeroPasajeros = numeroPasajeros;
    }

    public LocalDate getAnoModelo() {
        return anoModelo;
    }

    public void setAnoModelo(LocalDate anoModelo) {
        this.anoModelo = anoModelo;
    }

    public boolean getTipo() {
        return tipo;
    }

    public void setTipo(boolean tipo) {
        this.tipo = tipo;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    //To string que devuelve los campos principales
    @Override
    public String toString() {
        return "Modelo = " + modelo + " --> " +
                "Destino - " + destino;
    }

}
