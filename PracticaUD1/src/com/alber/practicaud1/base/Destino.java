package com.alber.practicaud1.base;

import java.io.Serializable;
import java.time.LocalDate;

public class Destino implements Serializable {
    //Clase que muestra unicamente las propiedades de el objeto
    String ciudadDestino;
    String pais;
    LocalDate fechaSalida;
    LocalDate fechaLlegada;
    int precio;
    Compania compania;

    public Destino() {
    }

    public Destino(String ciudadDestino, String pais, LocalDate fechaSalida, LocalDate fechaLlegada, int precio, Compania compania) {
        this.ciudadDestino = ciudadDestino;
        this.pais = pais;
        this.fechaSalida = fechaSalida;
        this.fechaLlegada = fechaLlegada;
        this.precio = precio;
        this.compania = compania;
    }

    public Destino(String ciudadDestino, String pais, LocalDate fechaSalida, LocalDate fechaLlegada, int precio) {
        this.ciudadDestino = ciudadDestino;
        this.pais = pais;
        this.fechaSalida = fechaSalida;
        this.fechaLlegada = fechaLlegada;
        this.precio = precio;
    }

    //Getters y Setters
    public String getCiudadDestino() {
        return ciudadDestino;
    }

    public void setCiudadDestino(String ciudadDestino) {
        this.ciudadDestino = ciudadDestino;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public LocalDate getFechaSalida() {
        return  fechaSalida;
    }

    public void setFechaSalida(LocalDate fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public LocalDate getFechaLlegada() {
        return fechaLlegada;
    }

    public void setFechaLlegada(LocalDate fechaLlegada) {
        this.fechaLlegada = fechaLlegada;
    }


    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getPrecio(){
        return precio;
    }

    public Compania getCompania() {
        return compania;
    }

    public void setCompania(Compania compania) {
        this.compania = compania;
    }

    //To string que devuelve los campos principales
    @Override
    public String toString() {
        return "Ciudad = " + ciudadDestino + " --> " +
                "Compañia - " + compania;
    }

}
