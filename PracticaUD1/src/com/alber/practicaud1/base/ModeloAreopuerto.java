package com.alber.practicaud1.base;

import org.w3c.dom.*;
import org.xml.sax.SAXException;
import sun.security.krb5.internal.crypto.Des;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.beans.*;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class ModeloAreopuerto {
    //Listas de cada objeto en ArrayList
    private ArrayList<Compania> companias;
    private ArrayList<Destino> destinos;
    private ArrayList<Avion> aviones;

    public ModeloAreopuerto() {
        companias = new ArrayList<>();
        destinos = new ArrayList<>();
        aviones = new ArrayList<>();
    }

    //Metodos de alta que crean el objeto nuevo con los campos que le e pasado y lo añade a la lista del objeto (ArrayList)
    public void altaAvion(String modelo, String id, int numeroPasajeros, LocalDate añoModelo, boolean tipo, Destino destino) {
        aviones.add(new Avion(modelo, id, numeroPasajeros, añoModelo, tipo, destino));
    }
    public void altaDestino(String ciudadDestino, String pais, LocalDate fechaSalida, LocalDate fechaLlegada, int precio, Compania compania) {
        destinos.add(new Destino(ciudadDestino, pais, fechaSalida, fechaLlegada, precio, compania));
    }
    public void altaCompania(String nombreCompania, String ciudad, int empleados, int valor, int clientes) {
        companias.add(new Compania(nombreCompania, ciudad, empleados, valor, clientes));
    }

    //Metodos que reciben el objeto que le e pasado y lo elimian de la lista (ArrayList)
    public void eliminarAvion(Avion avion) {
        aviones.remove(avion);
    }
    public void eliminarDestino(Destino destino) {
        destinos.remove(destino);
    }
    public void eliminarCompania(Compania compania) {
        companias.remove(compania);
    }

    //Los metodos de modificar reciben los campos y sustuyen estos por los actuales con setText(campo)
    public void modificarAvion(String modelo, String id, int numeroPasajeros, LocalDate anoModelo, boolean tipo, Destino destinoSeleccionado, Avion avion) {
        avion.setModelo(modelo);
        avion.setId(id);
        avion.setNumeroPasajeros(numeroPasajeros);
        avion.setAnoModelo(anoModelo);
        avion.setTipo(tipo);
    }
    public void modificarDestino(String ciudadDestino, String pais, LocalDate fechaSalida, LocalDate fechaLlegada,
                                 int precio, Destino destino, Compania companiaDestinoSeleccionada) {
        destino.setCiudadDestino(ciudadDestino);
        destino.setPais(pais);
        destino.setFechaSalida(fechaSalida);
        destino.setFechaLlegada(fechaLlegada);
        destino.setPrecio(precio);
        destino.setCompania(companiaDestinoSeleccionada);
    }
    public void modificarCompania(String nombreCompania, String ciudad, int empleados, int valor, int clientes, Compania compania) {
        compania.setNombreCompania(nombreCompania);
        compania.setCiudad(ciudad);
        compania.setEmpleados(empleados);
        compania.setValor(valor);
        compania.setClientes(clientes);
    }

    //Metodo encargado de recibir un fichero y escribir los datos en un fichero
    public void guardarDatos(File f) throws IOException {
        FileOutputStream fos = null;
        ObjectOutputStream oos;
        try {
            fos = new FileOutputStream(f);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(aviones);
            oos.writeObject(destinos);
            oos.writeObject(companias);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //Metodo que lee los datos para ser posteriormente cargados
    public void cargarDatos(File fichero) throws IOException, ClassNotFoundException {
        if (fichero.exists()) {
            FileInputStream fis = new FileInputStream(fichero);
            ObjectInputStream ois = new ObjectInputStream(fis);
            aviones = (ArrayList<Avion>) ois.readObject();
            destinos = (ArrayList<Destino>) ois.readObject();
            companias = (ArrayList<Compania>) ois.readObject();
            fis.close();
        } else {
            aviones = new ArrayList<>();
            destinos = new ArrayList<>();
            companias = new ArrayList<>();
        }
    }

    public void exportarXML(File selectedFile) throws ParserConfigurationException, TransformerException, IOException {
       /* DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        //Creamos el elemento documento <xml>
        Document documento = dom.createDocument(null, "xml", null);
        //Cramos el nodo padre (Como es el unico y el que alberga todos los demas nodos lo llamos raiz)
        Element raizAvion = documento.createElement("Aviones");
        documento.getDocumentElement().appendChild(raizAvion);
        Element raizDestino = documento.createElement("Destinos");
        documento.getDocumentElement().appendChild(raizDestino);
        Element raizCompania = documento.createElement("Compañias");
        documento.getDocumentElement().appendChild(raizCompania);

        //Creamos los distintos subnodos recorriendo con un for
        Element nodoModelo = null;
        Element nodoID = null;
        Element nodoNumPasajeros = null;
        Element nodoAnoModelo = null;
        Element nodoTipo = null;
        Element nodoCBDestino = null;
        Element nodoNombreDestino = null;
        Element nodoCiudadDestino = null;
        Element nodoPais = null;
        Element nodoFechaSalida = null;
        Element nodoFechaLlegada = null;
        Element nodoPrecio = null;
        Element nodoCBCompania = null;
        Element nodoNombreComp = null;
        Element nodoCiudadComp = null;
        Element nodoEmpleados = null;
        Element nodoValor = null;
        Element nodoClientes = null;

        Text dato = null;

        for (Avion avion : aviones) {
            //Añado todos los elementos al elemento raiz de Aviones
            nodoModelo = documento.createElement("Modelo");
            raizAvion.appendChild(nodoModelo);

            dato = documento.createTextNode(avion.getModelo());
            nodoModelo.appendChild(dato);

            nodoID = documento.createElement("ID");
            raizAvion.appendChild(nodoID);

            dato = documento.createTextNode(avion.getId());
            nodoID.appendChild(dato);

            nodoNumPasajeros = documento.createElement("NumeroDePasajeros");
            nodoNumPasajeros.appendChild(nodoNumPasajeros);

            dato = documento.createTextNode(String.valueOf(avion.getNumeroPasajeros()));
            nodoNumPasajeros.appendChild(dato);

            nodoAnoModelo = documento.createElement("AnoDeModelo");
            nodoAnoModelo.appendChild(nodoAnoModelo);

            dato = documento.createTextNode(String.valueOf(avion.getAnoModelo().toString()));
            nodoAnoModelo.appendChild(dato);

            nodoTipo = documento.createElement("Tipo");
            nodoTipo.appendChild(nodoTipo);

            dato = documento.createTextNode(String.valueOf(avion.getTipo()));
            nodoTipo.appendChild(dato);

            nodoCBDestino = documento.createElement("DestinoDeAvion");
            nodoCBDestino.appendChild(nodoCBDestino);

            dato = documento.createTextNode(String.valueOf(avion.getDestino()));
            nodoCBDestino.appendChild(dato);
        }

        for (Destino destino : destinos) {
            //Añado todos los elementos al elemento raiz de Destinos
            nodoNombreDestino = documento.createElement("NombreDestino");
            raizCompania.appendChild(nodoNombreDestino);

            dato = documento.createTextNode(destino.getCiudadDestino());
            nodoCiudadDestino.appendChild(dato);

            nodoPais = documento.createElement("Pais");
            raizDestino.appendChild(nodoPais);

            dato = documento.createTextNode(destino.getPais());
            nodoPais.appendChild(dato);

            nodoFechaSalida = documento.createElement("FechaSalida");
            nodoFechaSalida.appendChild(nodoFechaSalida);

            dato = documento.createTextNode(String.valueOf(destino.getFechaSalida().toString()));
            nodoFechaSalida.appendChild(dato);

            nodoFechaLlegada = documento.createElement("FechaLlegada");
            nodoFechaLlegada.appendChild(nodoFechaLlegada);

            dato = documento.createTextNode(String.valueOf(destino.getFechaLlegada().toString()));
            nodoFechaLlegada.appendChild(dato);

            nodoPrecio = documento.createElement("Precio");
            nodoPrecio.appendChild(nodoPrecio);

            dato = documento.createTextNode(String.valueOf(destino.getPrecio()));
            nodoPrecio.appendChild(dato);

            nodoCBCompania = documento.createElement("CompañiaDeDestino");
            nodoCBCompania.appendChild(nodoCBCompania);

            dato = documento.createTextNode(String.valueOf(destino.getCompania()));
            nodoCBCompania.appendChild(dato);
        }

        for (Compania compania : companias) {
            //Añado todos los elementos al elemento raiz de Compañias
            nodoNombreComp = documento.createElement("NombreCompania");
            raizCompania.appendChild(nodoNombreComp);

            dato = documento.createTextNode(compania.getNombreCompania());
            nodoNombreComp.appendChild(dato);

            nodoCiudadComp = documento.createElement("Ciudad");
            raizDestino.appendChild(nodoCiudadComp);

            dato = documento.createTextNode(compania.getCiudad());
            nodoCiudadComp.appendChild(dato);

            nodoEmpleados = documento.createElement("Empleados");
            nodoEmpleados.appendChild(nodoEmpleados);

            dato = documento.createTextNode(String.valueOf(compania.getEmpleados()));
            nodoEmpleados.appendChild(dato);

            nodoValor = documento.createElement("Valor");
            nodoValor.appendChild(nodoValor);

            dato = documento.createTextNode(String.valueOf(compania.getValor()));
            nodoValor.appendChild(dato);

            nodoClientes = documento.createElement("Clientes");
            nodoClientes.appendChild(nodoClientes);

            dato = documento.createTextNode(String.valueOf(compania.getClientes()));
            nodoClientes.appendChild(dato);
        }
        Source src = new DOMSource(documento);
        Result resultado = new StreamResult(selectedFile);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(src, resultado);*/

       //Crea un arrayList nuevo donde añado las listas
        ArrayList o = new ArrayList();
        o.add(companias);
        o.add(destinos);
        o.add(aviones);

        //Cojemos el fichero y gracias al metodo xmlEncoder cogemos la case LocalDate y la parseamos medianto un toString()
        FileOutputStream fos = new FileOutputStream(selectedFile);
        XMLEncoder xmlEncoder = new XMLEncoder(fos);
        xmlEncoder.setPersistenceDelegate(LocalDate.class, new PersistenceDelegate() {
            @Override
            protected Expression instantiate(Object localDate, Encoder out) {
                return new Expression(localDate, LocalDate.class, "parse", new Object[]{localDate.toString()});
            }
        });
        //Escribimos las listas y cerramos el xmlEncoder y el FileOutPutStream que guarda el fichero xml
        xmlEncoder.writeObject(o);
        xmlEncoder.close();
        fos.close();
    }

    public void importarXML(File selectedFile) throws ParserConfigurationException, IOException, SAXException {
        /*DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();

        Document document = builder.parse(selectedFile);

        NodeList nodoAviones = document.getElementsByTagName("Aviones");
        NodeList nodoDestino = document.getElementsByTagName("Destinos");
        NodeList nodoCompania = document.getElementsByTagName("Companias");

        for (int i = 0; i > nodoAviones.getLength(); i++) {
            Element avion = (Element) nodoAviones.item(i);
            String modelo = avion.getElementsByTagName("Modelo").item(0).getChildNodes().item(0).getTextContent();
            String id = avion.getElementsByTagName("ID").item(0).getChildNodes().item(0).getTextContent();
            String numeroPasajeros = avion.getElementsByTagName("NumeroDePasajeros").item(0).getChildNodes().item(0).getTextContent();
            String anoModelo = avion.getElementsByTagName("Ano de Modelo").item(0).getChildNodes().item(0).getTextContent();
            String tipo = avion.getElementsByTagName("Tipo").item(0).getChildNodes().item(0).getTextContent();
            String destino = avion.getElementsByTagName("DestinoDeAvion").item(0).getChildNodes().item(0).getTextContent();

           // altaAvion(modelo, id, Integer.parseInt(numeroPasajeros), LocalDate.parse(anoModelo), Boolean.parseBoolean(tipo), destino);
        }

        for (int i = 0; i > nodoDestino.getLength(); i++) {
            Element destino = (Element) nodoDestino.item(i);
            String nombreDestino = destino.getElementsByTagName("NombreDestino").item(0).getChildNodes().item(0).getTextContent();
            String pais = destino.getElementsByTagName("Pais").item(0).getChildNodes().item(0).getTextContent();
            String fechaSalida = destino.getElementsByTagName("FechaSalida").item(0).getChildNodes().item(0).getTextContent();
            String fechaLlegada = destino.getElementsByTagName("FechaLlegada").item(0).getChildNodes().item(0).getTextContent();
            String precio = destino.getElementsByTagName("Precio").item(0).getChildNodes().item(0).getTextContent();
            String compania = destino.getElementsByTagName("CompañiaDeDestino").item(0).getChildNodes().item(0).getTextContent();

          //  altaDestino(nombreDestino, pais, LocalDate.parse(fechaSalida), LocalDate.parse(fechaLlegada), Integer.parseInt(precio), compania);
        }

        for (int i = 0; i > nodoCompania.getLength(); i++) {
            Element compania = (Element) nodoCompania.item(i);
            String nombreCompania = compania.getElementsByTagName("NombreCompania").item(0).getChildNodes().item(0).getTextContent();
            String ciudad = compania.getElementsByTagName("Ciudad").item(0).getChildNodes().item(0).getTextContent();
            String empleados = compania.getElementsByTagName("Empleados").item(0).getChildNodes().item(0).getTextContent();
            String valor = compania.getElementsByTagName("Valor").item(0).getChildNodes().item(0).getTextContent();
            String clientes = compania.getElementsByTagName("Clientes").item(0).getChildNodes().item(0).getTextContent();


            altaCompania(nombreCompania, ciudad, Integer.parseInt(empleados),
                    Integer.parseInt(valor), Integer.parseInt(clientes));
        }*/

        //Primero importamos el fichero seleccionado
        FileInputStream fis = new FileInputStream(selectedFile);
        //Le implementamos el parseo del xmlDecoder al fichero seleccionado(fis)
        XMLDecoder decoder = new XMLDecoder(fis);
        //Creamos el objeto o que es el fichero xml parseado
        Object o = decoder.readObject();
        //Cerramos el xmlDecoder y el fichero
        decoder.close();
        fis.close();

        //Si el objeto o es compatible con el tipo ArrayList
        if (o instanceof ArrayList) {
            //Casteamos el objeto fichero a ArrayList
            ArrayList s = (ArrayList) o;
            //Si el objeto de la lista seleccionado no esta vacio lo incluye en el ArrayList  del objeto
            if (s.get(0) != null) {
                companias = (ArrayList<Compania>) s.get(0);
            }
            if (s.get(1) != null) {
                destinos = (ArrayList<Destino>) s.get(1);
            }
            if (s.get(2) != null) {
                aviones = (ArrayList<Avion>) s.get(2);
            }
        }
    }




    //Recibe la compañia seleccionada en el JList de compañia
    public ArrayList<Destino> getDestinosCompania(Compania compania) {
        //Lista de los destinos relacionados con la compañia
        ArrayList<Destino> nuevosDestinos = new ArrayList<>();
        //Recorro la lista de destinos
        for (Destino destino : this.destinos) {
            //Si la compañia seleccionada en la lista de destino es igual a la compañia que le hemos pasado
            if (destino.getCompania() != null && destino.getCompania().equals(compania)) {
                //Añadimos ese destino a la lista de destinos relacionados con la compañia
                nuevosDestinos.add(destino);
            }
        }
        //Devuelve la lista
        return nuevosDestinos;
    }

    //Recibe el destino seleccionado en el JList de destino
    public ArrayList<Avion> getAvionesDestino(Destino destino){
    //Lista de los aviones relacionados con el destino
        ArrayList<Avion> nuevosAviones = new ArrayList<>();
        //Recorro la lista de aviones
        for (Avion avion : this.aviones) {
            //Si el destino seleccionado en la lista de avion es igual a el destino que le hemos pasado
            if (avion.getDestino() != null && avion.getDestino().equals(destino)) {
                //Añadimos ese avion a la lista de aviones relacionados con el destino
                nuevosAviones.add(avion);
            }
        }
        //Devuelve la lista
        return nuevosAviones;
    }

    //Getters de las listas
    public ArrayList<Avion> getAviones() {
        return aviones;
    }

    public ArrayList<Compania> getCompanias() {
        return companias;
    }

    public ArrayList<Destino> getDestinos() {
        return destinos;
    }

}