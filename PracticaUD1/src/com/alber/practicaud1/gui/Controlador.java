package com.alber.practicaud1.gui;

import com.alber.practicaud1.base.Avion;
import com.alber.practicaud1.base.Compania;
import com.alber.practicaud1.base.Destino;
import com.alber.practicaud1.base.ModeloAreopuerto;
import org.xml.sax.SAXException;
import sun.security.krb5.internal.crypto.Des;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, WindowListener {
    Avion avion;
    Vista vista;
    ModeloAreopuerto modeloAeropuerto;

    public Controlador(Vista vista, ModeloAreopuerto modeloAeropuerto) {
        //Iniciamos las dos clases que vamos a manejar
        this.vista = vista;
        this.modeloAeropuerto = modeloAeropuerto;

        anadirActionListeners(this);
        anadirWindowListeners(this);
        //Cada vez que cargamos la ejecucion abrimos el fichero guardado la ultima vez
        File fichero = new File("aeropuerto.bin");
        try {
            modeloAeropuerto.cargarDatos(fichero);
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        refrescarListaAviones();
        refrescarComboBoxDestinos();
        refrescarListaDestino();
        refrescarComboBoxCompania();
        refrescarListaCompania();
    }

    //Añadimos los action listeners a todos los elementos funcionales
    private void anadirActionListeners(ActionListener listener) {
        vista.altaAvion.addActionListener(this);
        vista.eliminarAvion.addActionListener(this);
        vista.modificarAvion.addActionListener(this);

        vista.altaDestino.addActionListener(this);
        vista.eliminarDestino.addActionListener(this);
        vista.modificarDestino.addActionListener(this);
        vista.btnMostrarAviones.addActionListener(this);

        vista.altaCompania.addActionListener(this);
        vista.eliminarCompania.addActionListener(this);
        vista.modificarComp.addActionListener(this);
        vista.btnMostrarDestinos.addActionListener(this);

        vista.itemGuardar.addActionListener(this);
        vista.itemCargar.addActionListener(this);
        vista.itemImportar.addActionListener(this);
        vista.itemExportar.addActionListener(this);

        //Listener para la seleccion de elementos en la lista
        vista.listaAviones.addListSelectionListener(this);
        vista.listaCompania.addListSelectionListener(this);
        vista.listaDestino.addListSelectionListener(this);
    }

    //Listener de la ventana
    private void anadirWindowListeners(WindowListener listener) {
        vista.frame.addWindowListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        int clientes = 0;
        int valor = 0;
        int empleados = 0;
        int precio = 0;
        int pasajeros = 0;
        switch (comando) {
            case "altaCompania":
                try {
                    //Para que no puedas poner un String en un int le añadimos una excepcion
                    clientes = Integer.parseInt(vista.txtClientes.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el campo clientes tiene que ser un numero entero");
                    break;
                }

                try {
                    valor = Integer.parseInt(vista.txtValor.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el campo valor tiene que ser un numero entero");
                    break;
                }

                try {
                    empleados = Integer.parseInt(vista.txtEmpleados.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el campo empleados tiene que ser un numero entero");
                    break;
                }
                //Codigo referente al evento del boton alta
                modeloAeropuerto.altaCompania(vista.txtNombre.getText(), vista.txtCiudadCompañia.getText(), empleados,
                        valor, clientes);
                //Metodo que borra los JTextField una vez dados de alta a la lista
                borrarCamposTextoCompania();
                break;
            case "altaDestino":
                try {
                    precio = Integer.parseInt(vista.txtPrecio.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "El precio tiene que ser un numero entero");
                    break;
                }
                modeloAeropuerto.altaDestino(vista.txtCiudadDestino.getText(), vista.txtPais.getText(), vista.dpFechaSalida.getDate(),
                        vista.dpFechaLlegada.getDate(), precio, (Compania) vista.dcbmCompania.getSelectedItem());
                borrarCamposTextoDestino();
                break;
            case "altaAvion":

                try {
                    pasajeros = Integer.parseInt(vista.txtNumeroPasajeros.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "Los pasajeros tienen que ser un numero entero");
                    break;
                }
                modeloAeropuerto.altaAvion(vista.txtModelo.getText(), vista.txtId.getText(), pasajeros,
                        vista.dpAnoModelo.getDate(), Boolean.parseBoolean(vista.txtTipo.getText()), (Destino) vista.dcbmDestino.getSelectedItem());
                borrarCamposTextoAvion();
                break;

            case "eliminarCompania":
                //Metodo que simplemente coge el elemento seleccionado y lo elimina de la lista
                Compania eliminadoCompania = (Compania) vista.listaCompania.getSelectedValue();
                modeloAeropuerto.eliminarCompania(eliminadoCompania);

                borrarCamposTextoCompania();
                break;
            case "eliminarDestino":
                Destino eliminadoDestino = (Destino) vista.listaDestino.getSelectedValue();
                modeloAeropuerto.eliminarDestino(eliminadoDestino);
                borrarCamposTextoDestino();
                break;
            case "eliminarAvion":
                Avion eliminadoAvion = (Avion) vista.listaAviones.getSelectedValue();
                modeloAeropuerto.eliminarAvion(eliminadoAvion);

                borrarCamposTextoAvion();
                break;

            case "modificarAvion":
                try {
                    pasajeros = Integer.parseInt(vista.txtNumeroPasajeros.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "Los pasajeros tienen que ser un numero entero");
                    break;
                }
                //Creamos unas variables para el valor seleccionado en la lista y en el comboBox
                Avion avionModificado = (Avion) vista.listaAviones.getSelectedValue();
                Destino destinoSeleccionado = (Destino) vista.cbDestinoAvion.getSelectedItem();
                //Escribimos los nuevos campos
                modeloAeropuerto.modificarAvion(vista.txtModelo.getText(), vista.txtId.getText(),
                        pasajeros, vista.dpAnoModelo.getDate(),
                        Boolean.parseBoolean(vista.txtTipo.getText()), destinoSeleccionado, avionModificado);
                //Y volvemos a escribir la lista con los cambios realizados
                this.refrescarListaAviones();

                break;

            case "modificarDestino":
                try {
                    precio = Integer.parseInt(vista.txtPrecio.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el precio tiene que ser un numero entero");
                    break;
                }
                Destino destinoModificado = (Destino) vista.listaDestino.getSelectedValue();
                Compania companiaDestinoSeleccionada = (Compania) vista.cbCompaniaDestino.getSelectedItem();

                modeloAeropuerto.modificarDestino(vista.txtCiudadDestino.getText(), vista.txtPais.getText(),
                        vista.dpFechaSalida.getDate(), vista.dpFechaLlegada.getDate(),
                        precio, destinoModificado, companiaDestinoSeleccionada);
                break;
            case "modificarCompania":
                try {
                    clientes = Integer.parseInt(vista.txtClientes.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el campo clientes tiene que ser un numero entero");
                    break;
                }

                try {
                    valor = Integer.parseInt(vista.txtValor.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el campo valor tiene que ser un numero entero");
                    break;
                }

                try {
                    empleados = Integer.parseInt(vista.txtEmpleados.getText());
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(vista, "el campo empleados tiene que ser un numero entero");
                    break;
                }
                Compania companiaSeleccionada = (Compania) vista.listaCompania.getSelectedValue();
                modeloAeropuerto.modificarCompania(vista.txtNombre.getText(), vista.txtCiudadCompañia.getText(),
                        empleados, valor
                        , clientes, companiaSeleccionada);
                break;

            case "mostrarAviones":
                //Si la lista de destinos no esta vacia
                if (vista.listaDestino.isSelectionEmpty()) {
                    JOptionPane.showMessageDialog(null, "Debes seleccionar un destino", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    /*Muestra el dialogo pasandole el valor seleccionado de la lista destinos y con el metodo getAvionesDestinos
                    restringimos los aviones mostrados a los que estan dentro de destinos */
                    MostrarAviones mostrarAviones = new MostrarAviones();
                    int resultado = mostrarAviones.mostrarDialogo(modeloAeropuerto.getAvionesDestino((Destino) vista.listaDestino.getSelectedValue()));
                }
                break;
            case "mostrarDestinos":
                if (vista.listaCompania.isSelectionEmpty()) {
                    JOptionPane.showMessageDialog(null, "Debes seleccionar una compañia", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                    MostrarDestinos mostrarDestinos = new MostrarDestinos();
                    int resultado = mostrarDestinos.mostrarDialogo(modeloAeropuerto.getDestinosCompania((Compania) vista.listaCompania.getSelectedValue()));
                }
                break;

                //Guardar Fichero
            case "Guardar": {
                JFileChooser selector = new JFileChooser();
                int opt = selector.showSaveDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    File fichero = selector.getSelectedFile();
                    try {
                        modeloAeropuerto.guardarDatos(fichero);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            break;

            //Cargar Fichero
            case "Cargar": {
                JFileChooser selector = new JFileChooser();
                int opt = selector.showOpenDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    File fichero = selector.getSelectedFile();
                    try {
                        modeloAeropuerto.cargarDatos(fichero);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            break;

            //Exportar XML
            case "Exportar":
                JFileChooser selector = new JFileChooser();

                selector.setFileFilter(new FileNameExtensionFilter("Ficheros XML", "XML"));

                int opt = selector.showOpenDialog(null);

                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modeloAeropuerto.exportarXML(selector.getSelectedFile());

                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (TransformerException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                break;

                //Importar XML
            case "Importar":
                JFileChooser selector2 = new JFileChooser();

                selector2.setFileFilter(new FileNameExtensionFilter("Ficheros XML", "XML"));

                int opt2 = selector2.showSaveDialog(null);

                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modeloAeropuerto.importarXML(selector2.getSelectedFile());
                    } catch (ParserConfigurationException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (SAXException e1) {
                        e1.printStackTrace();
                    }
                }
                break;
        }
        refrescarListaCompania();
        refrescarComboBoxCompania();
        refrescarListaDestino();
        refrescarComboBoxDestinos();
        refrescarListaAviones();
    }


    private void borrarCamposTextoCompania() {
        vista.txtNombre.setText("");
        vista.txtCiudadCompañia.setText("");
        vista.txtEmpleados.setText("");
        vista.txtValor.setText("");
        vista.txtClientes.setText("");
    }

    private void borrarCamposTextoDestino() {
        vista.txtCiudadDestino.setText("");
        vista.txtPais.setText("");
        vista.dpFechaSalida.setText("");
        vista.dpFechaLlegada.setText("");
        vista.txtPrecio.setText("");
    }

    private void borrarCamposTextoAvion() {
        vista.txtModelo.setText("");
        vista.txtId.setText("");
        vista.txtNumeroPasajeros.setText("");
        vista.dpAnoModelo.setText("");
        vista.txtTipo.setText("");
    }

    private void refrescarListaAviones() {
        vista.dlmAvion.clear();
        for (Avion avion : modeloAeropuerto.getAviones()) {
            vista.dlmAvion.addElement(avion);
        }
    }

    private void refrescarListaDestino() {
        vista.dlmDestino.clear();
        for (Destino destino : modeloAeropuerto.getDestinos()) {
            vista.dlmDestino.addElement(destino);
        }
    }

    private void refrescarListaCompania() {
        vista.dlmCompania.clear();
        for (Compania compania : modeloAeropuerto.getCompanias()) {
            vista.dlmCompania.addElement(compania);
        }
    }

    private void refrescarComboBoxDestinos() {
        vista.dcbmDestino.removeAllElements();
        //Anado un etrenador null, por si no se quiere seleccionar destinos
        vista.dcbmDestino.addElement(null);
        for (Destino destino : modeloAeropuerto.getDestinos()) {
            vista.dcbmDestino.addElement(destino);
        }
    }

    private void refrescarComboBoxCompania() {
        vista.dcbmCompania.removeAllElements();
        //Anado un etrenador null, por si no se quiere seleccionar compañias
        vista.dcbmCompania.addElement(null);
        for (Compania compania : modeloAeropuerto.getCompanias()) {
            vista.dcbmCompania.addElement(compania);
        }
    }


    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
        try {
            modeloAeropuerto.guardarDatos(new File("aeropuerto.bin"));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    //Metodo que por el nombre de la lista distingue entre ellas para individualmente mostrar la informacion de cada campo
    //Al seleccionar cada objeto listado
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            JList lista = (JList) e.getSource();
            switch (lista.getName()) {
                case "ListaAviones":
                    Avion a = (Avion) lista.getSelectedValue();
                    mostrarInfoAvion(a);
                    break;
                case "ListaDestion":
                    Destino d = (Destino) lista.getSelectedValue();
                    mostrarInfoDestino(d);
                    break;
                case "listaCompania":
                    Compania c = (Compania) lista.getSelectedValue();
                    mostrarInfoCompania(c);
                    break;
            }
        }
    }

    private void mostrarInfoAvion(Avion a) {
        vista.txtModelo.setText(a.getModelo());
        vista.txtId.setText(a.getId());
        vista.txtNumeroPasajeros.setText(Integer.toString(a.getNumeroPasajeros()));
        vista.dpAnoModelo.setDate(a.getAnoModelo());
        vista.txtTipo.setText(Boolean.toString(a.getTipo()));
        vista.cbDestinoAvion.setSelectedItem(a.getDestino());
    }

    private void mostrarInfoDestino(Destino d) {
        vista.txtCiudadDestino.setText(d.getCiudadDestino());
        vista.txtPais.setText(d.getPais());
        vista.dpFechaSalida.setDate(d.getFechaSalida());
        vista.dpFechaLlegada.setDate(d.getFechaLlegada());
        vista.txtPrecio.setText(Integer.toString(d.getPrecio()));
        vista.cbCompaniaDestino.setSelectedItem(d.getCompania());
    }

    private void mostrarInfoCompania(Compania c) {
        vista.txtNombre.setText(c.getNombreCompania());
        vista.txtCiudadCompañia.setText(c.getCiudad());
        vista.txtEmpleados.setText(Integer.toString(c.getEmpleados()));
        vista.txtValor.setText(Integer.toString(c.getValor()));
        vista.txtClientes.setText(Integer.toString(c.getClientes()));
    }
}
