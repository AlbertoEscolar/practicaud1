package com.alber.practicaud1.gui;

import com.alber.practicaud1.base.Avion;

import javax.swing.*;
import java.awt.event.*;
import java.util.List;

public class MostrarAviones extends JDialog {
    public final static  int ACEPTAR=1;
    public final static  int CANCELAR=0;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList list1;

    private DefaultListModel<Avion> dlmAviones;
    private int estado;
    private List<Avion> aviones;
    private List<Avion> avionesSeleccionados;

    public MostrarAviones() {
        //Inicializamos la lista
        dlmAviones = new DefaultListModel<>();
        list1.setModel(dlmAviones);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        setLocationRelativeTo(null);

        pack();
    }

    private void onOK() {
        estado = ACEPTAR;
        avionesSeleccionados = list1.getSelectedValuesList();

        dispose();
    }

    private void onCancel() {
        estado= CANCELAR;
        dispose();
    }
    public int mostrarDialogo(List<Avion> aviones) {
        this.aviones=aviones;
        mostrarAviones();

        setVisible(true);
        return estado;
    }

    private void mostrarAviones() {
        dlmAviones.clear();

        for (Avion avion : aviones){
            dlmAviones.addElement(avion);
        }
    }

    public List<Avion> obtenerAvionesSeleccionados() {
        return avionesSeleccionados;
    }
}