package com.alber.practicaud1.gui;

import com.alber.practicaud1.base.Avion;
import com.alber.practicaud1.base.Compania;
import com.alber.practicaud1.base.Destino;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

public class Vista extends JFrame{
    //Creación de los campos de la vista sin el private para cogerlos con otras clases
     JPanel panel1;
     JTextField txtNombre;
     JTextField txtCiudadCompañia;
     JTextField txtEmpleados;
     JTextField txtValor;
     JTextField txtClientes;
     JButton altaCompania;
     JButton eliminarCompania;
     JButton modificarComp;
     JButton altaDestino;
     JButton eliminarDestino;
     JButton modificarDestino;
     JTextField txtCiudadDestino;
     JTextField txtPais;
     JTextField txtPrecio;
     JTextField txtModelo;
     JTextField txtId;
     JTextField txtNumeroPasajeros;
     JTextField txtTipo;
     JButton altaAvion;
     JButton eliminarAvion;
     JButton modificarAvion;
     JList listaCompania;
     JList listaDestino;
     JList listaAviones;
     JButton btnMostrarAviones;
     JButton btnMostrarDestinos;
     JComboBox cbDestinoAvion;
     JComboBox cbCompaniaDestino;
     DatePicker dpFechaSalida;
     DatePicker dpFechaLlegada;
     DatePicker dpAnoModelo;
     JFrame frame;
     JMenuItem itemGuardar;
     JMenuItem itemCargar;
     JMenuItem itemExportar;
     JMenuItem itemImportar;
     DefaultComboBoxModel<Destino> dcbmDestino;
     DefaultComboBoxModel<Compania> dcbmCompania;
     DefaultListModel<Compania> dlmCompania;
     DefaultListModel<Destino> dlmDestino;
     DefaultListModel<Avion> dlmAvion;

    public Vista(Controlador controlador) {
        //Propiedades de la ventana
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Ventana en el medio
        setLocationRelativeTo(null);
        //Iniciamos los DefaultListModel
        iniciarModelos();
        //Añadimos el menu
        crearMenu();
        //Comprime la ventana y la hace visible
        frame.pack();
        frame.setVisible(true);
    }

    private void iniciarModelos() {
        //Iniciamos las DefaultListModel
        dlmCompania = new DefaultListModel<>();
        dlmDestino = new DefaultListModel<>();
        dlmAvion = new DefaultListModel<>();

        dcbmDestino = new DefaultComboBoxModel<>();
        dcbmCompania = new DefaultComboBoxModel<>();

        //Asignamos el modelo a las JList con los DefaultModelList y les ponemos un nombre de identificacion
        listaCompania.setModel(dlmCompania);
        listaCompania.setName("listaCompania");
        listaDestino.setModel(dlmDestino);
        listaDestino.setName("ListaDestion");
        listaAviones.setModel(dlmAvion);
        listaAviones.setName("ListaAviones");
        //Asignamos el modelo a los combo box
        cbDestinoAvion.setModel(dcbmDestino);
        cbCompaniaDestino.setModel(dcbmCompania);
    }

    private void crearMenu(){
        //Creamos la barra de menu
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemGuardar = new JMenuItem("Guardar");
        itemCargar = new JMenuItem("Cargar");
        itemImportar = new JMenuItem("Importar XML");
        itemExportar = new JMenuItem("Exportar XML");
        //Anado los actions commands a estos dos botones
        itemCargar.setActionCommand("Cargar");
        itemGuardar.setActionCommand("Guardar");
        itemImportar.setActionCommand("Importar");
        itemExportar.setActionCommand("Exportar");

        menu.add(itemCargar);
        menu.add(itemGuardar);
        menu.add(itemImportar);
        menu.add(itemExportar);

        barra.add(menu);

        frame.setJMenuBar(barra);
    }
}
