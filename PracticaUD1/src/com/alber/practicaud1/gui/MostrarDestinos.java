package com.alber.practicaud1.gui;

import com.alber.practicaud1.base.Avion;
import com.alber.practicaud1.base.Destino;

import javax.swing.*;
import java.awt.event.*;
import java.util.List;

public class MostrarDestinos extends JDialog {
    public final static  int ACEPTAR=1;
    public final static  int CANCELAR=0;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JList list1;

    private DefaultListModel<Destino> dlmDestino;
    private int estado;
    private List<Destino> destinos;
    private List<Destino> destinosSelecionados;

    public MostrarDestinos() {
        //Inicializamos la lista
        dlmDestino = new DefaultListModel<>();
        list1.setModel(dlmDestino);

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        setLocationRelativeTo(null);

        pack();
    }

    private void onOK() {
        estado = ACEPTAR;
        dispose();
    }

    private void onCancel() {
        estado= CANCELAR;
        dispose();
    }
    public int mostrarDialogo(List<Destino> destinos) {
        this.destinos=destinos;
        mostrarDestinos();

        setVisible(true);
        return estado;
    }

    private void mostrarDestinos() {
        dlmDestino.clear();

        for (Destino destino : destinos){
            dlmDestino.addElement(destino);
        }
    }

    public List<Destino> obtenerDestinosSeleccionados() {
        return destinosSelecionados;
    }
}